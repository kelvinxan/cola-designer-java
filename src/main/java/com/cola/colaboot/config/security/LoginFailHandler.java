package com.cola.colaboot.config.security;

import com.alibaba.fastjson.JSONObject;
import com.cola.colaboot.config.dto.Res;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class LoginFailHandler implements AuthenticationFailureHandler {
    /**  
     * 登录失败执行.
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException{
        PrintWriter writer = response.getWriter();
        writer.write(JSONObject.toJSONString(Res.fail("登录失败："+e.getMessage())));
        writer.close();
    }
}
